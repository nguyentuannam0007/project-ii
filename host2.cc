///*
// * host.cc
// *
// *  Created on: May 7, 2020
// *      Author: Admin
// */
//#include<omnetpp.h>
//#include<string.h>
//#include<stdio.h>
//#include<stdlib.h>
//#include "myMessage_m.h"
//
//using namespace std;
//using namespace omnetpp;
//
//
//
//
//// Source queue
//struct SQ {
//    int idHead = 0;
//    int idTail = 0;
//
//    int getHeader(){
//        int ans = -1;
//        if (idHead > 0){
//            ans = idHead;
//            if (idTail == 0){
//                idHead = 0;
//            }
//            idHead++;
//            idTail--;
//        }
//        return ans;
//    }
//};
//
//// Exit buffer
//struct EB {
//    int *exitBuff;
//    int avaiLength = 2;
//};
//
//
//
//
//class Host: public cSimpleModule {
//private:
//    // Source queue and Exit buffer in here
//    SQ sq; // Source queue
//    EB eb; // Exit buffer
//
//    // For count message
//    int counter_msg = 0;
//
//    // For check channel is busy or net
//    bool isBusyChannel = false;
//
//    int *msgForInterval;   // For destination node: count message in interval times;
//    int lengthOfMsgForInterval;
//    double timeForGenMess; // For source node: time for generate message
//    double timeSimulation, intervalTime; // Time for simulation
//    double lastTimeSendMsg = 0;
//    double timDelayMsg = 0;
//
//    HostMessage * genMsg;
//    HostMessage * delTimeMsg;
//protected:
//    void handleMessage(cMessage *msg) override;
//    void initialize() override;
//    void finish() override;
//    HostMessage* generateMessage(int idMsg);
//
//    void insertToSQ(int idMsg);
//    void insertToEB();
//    void sendMessageInEB();
//};
//
//Define_Module(Host);
//
//void Host::initialize()
//{
//    // Get simulation's time
//    timeSimulation = (double) getParentModule()->par("simulationTime") / 1000;
//    intervalTime = (double) getParentModule()->par("intervalTime") / 1000;
//    timDelayMsg = (double) gate("out")->getChannel()->par("delay");
//
//    isBusyChannel = false;
//    if (strcmp(getName(), "hostB") == 0){ // For destination node
//
//        lengthOfMsgForInterval = (int)(timeSimulation / intervalTime);
//        msgForInterval = (int *)calloc(lengthOfMsgForInterval, sizeof(int));
//
//    }else{ // For begin node
//
//        eb.exitBuff = (int *)calloc(eb.avaiLength, sizeof(int));
//        timeForGenMess = (double)getParentModule()->par("cycleTimeGenMsg") / 1000;
//
//        // Generate a message
//        insertToSQ(++counter_msg);
//
//        // Generate a message for generate message after cycle generate message
//        genMsg = new HostMessage();
//        genMsg->setId(-1);
//        scheduleAt(simTime().dbl() + timeForGenMess, genMsg);
//
//        delTimeMsg = new HostMessage();
//        delTimeMsg->setId(-1);
//        scheduleAt(simTime().dbl(), delTimeMsg);
//    }
//}
//
//void Host::handleMessage(cMessage *msg)
//{
//
//    if (simTime().dbl() >= timeSimulation) return;
//
//    HostMessage * hostMsg = check_and_cast<HostMessage *>(msg);
//    if (hostMsg == genMsg){ // Generate a new message
//        insertToSQ(++counter_msg);
//        scheduleAt(timeForGenMess + simTime().dbl(), hostMsg);
//    }else {
//        if (strcmp(getName(), "hostB") == 0){ // Is destination;
//           long indexTimeInterval = (simTime().dbl() / intervalTime - 1);
//           EV << "simTime: " << simTime().dbl() << "\n";
//           EV << "intervalTime: " << intervalTime << "\n";
//           EV << "kq:" << simTime().dbl() / intervalTime << "\n";
//           EV << "indexTimeInterval: " << indexTimeInterval << "\n";
//           msgForInterval[indexTimeInterval] += 1;
//           delete hostMsg;
//        }else if (hostMsg == delTimeMsg){
//            EV <<"a: " << simTime().dbl() - lastTimeSendMsg << "\n";
//            EV <<"b: " << timDelayMsg << "\n";
//            if(simTime().dbl() - lastTimeSendMsg  >= timDelayMsg){
//                EV << "c: " << eb.avaiLength << "\n";
//                isBusyChannel = false;
//                sendMessageInEB();
//            }
//            scheduleAt(simTime().dbl() + 0.0001, hostMsg);
//        }
//    }
//}
//
//
//HostMessage* Host::generateMessage(int idMsg)
//{
//    HostMessage* msg = new HostMessage();
//    msg->setId(idMsg);
//
//    return msg;
//}
//
//
//void Host::insertToSQ(int idMsg)
//{
//    if (sq.idHead == 0){
//        sq.idHead = idMsg;
//    }else{
//        sq.idTail = idMsg;
//    }
//
//    insertToEB();
//}
//
//void Host::insertToEB()
//{
//    if (eb.avaiLength == 1){
//        int idMsg = sq.getHeader();
//        if(idMsg > 0){ // Insert to EB
//            eb.exitBuff[1] = idMsg;
//            eb.avaiLength--;
//        }
//    }else if (eb.avaiLength == 2){
//        int idMsg = sq.getHeader();
//        if (idMsg > 0){ // Insert to SB 0
//            eb.exitBuff[0] = idMsg;
//            eb.avaiLength--;
//            int idSeMsg = sq.getHeader();
//            if (idSeMsg > 0){
//                eb.exitBuff[1] = idSeMsg;
//                eb.avaiLength--;
//            }
//        }
//    }
//    sendMessageInEB();
//}
//
//void Host::sendMessageInEB()
//{
//    if (eb.avaiLength < 2){ // Check if exists any message in Exit buffer
//        if (!isBusyChannel){
//            // Listener channel: is busy or not
//            isBusyChannel = true;
//            lastTimeSendMsg = simTime().dbl();
//            EV << "Interval " << (int)(simTime().dbl() / intervalTime)<< "\n";
//            HostMessage *msg = generateMessage(eb.exitBuff[0]);
//            send(msg, "out");
//            EV << "LastTimeSendMsg: " << lastTimeSendMsg << "\n";
//
//            eb.exitBuff[0] = eb.exitBuff[1];
//            eb.exitBuff[1] = 0;
//            eb.avaiLength++;
//        }
//    }
//}
//
//
//void Host::finish()
//{
//    if (strcmp(getName(), "hostB") == 0)
//    {
//        int count_msg = 0;
//        for (int i = 0; i < lengthOfMsgForInterval; i++){
//            EV << "Interval Time " << i + 1 << " : " << msgForInterval[i] << "\n";
//            count_msg += msgForInterval[i];
//        }
//        EV << "Number of Msg: " << count_msg << "\n";
//    }
//
//}
//
